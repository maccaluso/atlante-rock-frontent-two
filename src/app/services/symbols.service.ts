import { Injectable } from '@angular/core';

@Injectable()
export class SymbolsService {

    private shapes: any;

    constructor() {}

    getShapes() { return this.shapes; }

    getSymbol(
        name: string,
        fillColor: string,
        fillOpacity: string = '1',
        strokeColor: string = '#000000',
        strokeOpacity: string = '0',
        strokeWidth: string = '1'
    ) {
        switch ( name ) {
            case 'square':
                return `
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                        <rect x="0" y="0" width="30" height="30" rx="2" 
                            fill="${fillColor}" 
                            fill-opacity="${fillOpacity}" 
                            stroke="${strokeColor}" 
                            stroke-opacity="${strokeOpacity}" 
                            stroke-width="${strokeWidth}" 
                        />
                    </svg>
                `;
                break;
            case 'circle':
                return `
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                        <circle cx="15" cy="15" r="15" 
                            fill="${fillColor}" 
                            fill-opacity="${fillOpacity}" 
                            stroke="${strokeColor}" 
                            stroke-opacity="${strokeOpacity}" 
                            stroke-width="${strokeWidth}" 
                        />
                    </svg>
                `;
                break;
            case 'rhombus':
                return `
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                        <polygon 
                            fill="${fillColor}" 
                            fill-opacity="${fillOpacity}" 
                            stroke="${strokeColor}" 
                            stroke-opacity="${strokeOpacity}" 
                            stroke-width="${strokeWidth}" 
                            points="15,0 30,15 15,30 0,15" 
                            stroke-linejoin="round" 
                            stroke-linecap="round" 
                        />
                    </svg>
                `;
                break;
            case 'hexagon':
                return `
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                        <polygon 
                            fill="${fillColor}" 
                            fill-opacity="${fillOpacity}" 
                            stroke="${strokeColor}" 
                            stroke-opacity="${strokeOpacity}" 
                            stroke-width="${strokeWidth}" 
                            points="15,0 30,7 30,23 15,30 0,23 0,7" 
                            stroke-linejoin="round" 
                            stroke-linecap="round" 
                        />
                    </svg>
                `;
                break;
            case 'triangle':
                return `
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                        <polygon 
                            fill="${fillColor}" 
                            fill-opacity="${fillOpacity}" 
                            stroke="${strokeColor}" 
                            stroke-opacity="${strokeOpacity}" 
                            stroke-width="${strokeWidth}" 
                            points="15,2 30,28 0,28" 
                            stroke-linejoin="round" 
                            stroke-linecap="round" 
                        />
                    </svg>
                `;
                break;
            case 'cross':
                // console.log('cross')
                return `
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                        <line x1="7.5" y1="7.5" x2="22.5" y2="22.5" 
                            stroke="${fillColor}" 
                            stroke-opacity="1" 
                            stroke-width="10" 
                            stroke-linejoin="round" 
                            stroke-linecap="round" 
                        />
                        <line x1="22.5" y1="7.5" x2="7.5" y2="22.5" 
                            stroke="${fillColor}" 
                            stroke-opacity="1" 
                            stroke-width="10" 
                            stroke-linejoin="round" 
                            stroke-linecap="round" 
                        />
                    </svg>
                `;
                break;
        }
    }
}