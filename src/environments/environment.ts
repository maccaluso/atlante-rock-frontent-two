// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDq112LMAgKXcqU1xqz2eQkPH4x0aQ9Bwg",
    authDomain: "atlante-rock.firebaseapp.com",
    databaseURL: "https://atlante-rock.firebaseio.com",
    projectId: "atlante-rock",
    storageBucket: "atlante-rock.appspot.com",
    messagingSenderId: "128304408510"
  },
  mapbox: {
    accessToken: 'pk.eyJ1IjoibG9wZXptYXBzIiwiYSI6ImNqNDB3YnJtcDAxNDMzMnMyNWZmMGZ3a3IifQ.2VbIS4DW-DHklfXmg1vlKw'
  },
  cloudinary: {
    cloud_name: 'gianluca-macaluso', 
    upload_preset: 'ROCK-Atlas'
  }
};
