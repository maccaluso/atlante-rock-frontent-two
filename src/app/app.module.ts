import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';

// import { AngularResizedEventModule } from 'angular-resize-event';

import { AppComponent } from './app.component';
import { MapComponent } from './components/map/map.component';

import { DataService } from './services/data.service';
import { SymbolsService } from './services/symbols.service';
import { HeaderComponent } from './components/header/header.component';
import { StoryComponent } from './components/story/story.component';
import { StoryModalComponent } from './components/story-modal/story-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    HeaderComponent,
    StoryComponent,
    StoryModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    CloudinaryModule.forRoot({Cloudinary}, environment.cloudinary as CloudinaryConfiguration),
    // AngularResizedEventModule
  ],
  providers: [
    DataService,
    SymbolsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
