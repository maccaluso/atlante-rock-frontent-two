import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { trigger, style, animate, transition } from '@angular/animations';

import { Subscription } from 'rxjs';

import { environment } from '../../../environments/environment';

import * as mapboxgl from 'mapbox-gl';
import distance from '@turf/distance';
import centroid from '@turf/centroid';

import { DataService } from '../../services/data.service';
import { SymbolsService } from '../../services/symbols.service';
import { StoryModalComponent } from '../story-modal/story-modal.component';
import { HeaderComponent } from '../header/header.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  animations: [
    trigger(
      'preloaderAnimation', [
        transition(':enter', [
          style( { opacity: 0 } ),
          animate( '500ms', style( { opacity: 1 } ) )
        ]),
        transition(':leave', [
          style( { opacity: 1 } ),
          animate( '500ms', style({ opacity: 0 } ) )
        ])
      ]
    )
  ],
})
export class MapComponent implements OnInit, OnDestroy {
  @ViewChild(StoryModalComponent) storyModal: StoryModalComponent;
  @ViewChild(HeaderComponent) header: HeaderComponent;

  public loading = true;
  public sidebarOpen = false;
  public sidebar2Open = false;
  private rockMode = true;
  private currentCity: any;
  public currentItem: any;
  private showingItem = false;

  public isModalOpen = false;
  public isModalLarge = false;

  private cityMarkers: any = [];
  private actionMarkers: any = [];
  private contextMarkers: any = [];
  private practiceMarkers: any = [];
  private currentAreaPopup: mapboxgl.Popup;

  public map: mapboxgl.Map;
  private mapContainerSelector = 'map';
  private styleUrl = 'mapbox://styles/lopezmaps/cj9fpbnby0dpa2roeayvnkfc5';
  private zoom = 3.5;
  private itemLevelZoom = 15;
  private cityMarkersVisibilityZoomThreshold = 10;
  private defaultLocation: number[] = [10.445716025696328, 48.92173309689147];
  public mapResizing = false;

  private subCounter = 0;
  private numSubs = 6;
  public data: any = {};
  public dataTree: any = [
    {
        name: 'contexts',
        singular: 'context',
        label: 'Urban context',
        categories: []
    },
    {
        name: 'actions',
        singular: 'action',
        label: 'ROCK actions',
        categories: []
    },
    {
        name: 'practices',
        singular: 'practice',
        label: 'Model practices',
        categories: []
    }
  ];
  private switches: any = [];
  private collapses: any = [];
  private selects: any = [false, false, false];
  private hiddenSlugs: string[] = []

  private citySub: Subscription = new Subscription();
  private modelPracticesSub: Subscription = new Subscription();
  private categoriesSub: Subscription = new Subscription();
  private typesSub: Subscription = new Subscription();
  private actionsSub: Subscription = new Subscription();
  private contextsSub: Subscription = new Subscription();

  private fieldSorter = (fields) => (a, b) => fields.map(o => {
    let dir = 1;
    if (o[0] === '-') { dir = -1; o = o.substring(1); }
    return a[o] > b[o] ? dir : a[o] < b[o] ? -(dir) : 0;
  }).reduce((p: any, n: any) => p ? p : n, 0);

  constructor(
    private dataSvc: DataService,
    private symbolsSvc: SymbolsService,
    private sanitizer: DomSanitizer
  ) {
    try {
      (mapboxgl as any).accessToken = environment.mapbox.accessToken;
    } catch ( err ) {
      console.warn( err );
    }
  }

  ngOnInit() {
    this.map = new mapboxgl.Map({
      container: this.mapContainerSelector,
      style: this.styleUrl,
      zoom: this.zoom,
      center: this.defaultLocation
    });

    this.map.on('mousemove', 'areaFill', (e: MouseEvent) => {
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', 'areaFill', (e: MouseEvent) => {
      this.map.getCanvas().style.cursor = '';
    });
    this.map.on('click', 'areaFill', (e: any) => {
      const geom = e.features[0].geometry.coordinates;
      const coordinates = this.getCentroidBase( geom ).slice();
      const props = e.features[0].properties;

      while ( Math.abs( e.lngLat.lng - coordinates[0] ) > 180 ) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }

      let html = '';
      html += '<div class="popup-inner">';
        html += '<h1>' + props.name + '</h1>';
        html += '<h3 style="color: ' + props.color + '">' + props.catName + '</h3>';
        if ( props.summary ) {
          html += '<p class="popup-summary">' + props.summary + '</p>';
        }
        if ( props.description ) {
          html += '<p class="popup-description">' + props.description + '</p>';
        }
      html += '</div>';

      this.currentAreaPopup = new mapboxgl.Popup()
        .setLngLat( coordinates )
        .setHTML( html )
        .addTo( this.map );
    });

    this.map.on('mousemove', 'areaStrokeInner', (e: any) => {
      if ( e.features && e.features[0] && e.features[0].properties.catSlug !== 'rock-area') {
        this.map.getCanvas().style.cursor = 'pointer';
      }
    });
    this.map.on('mouseleave', 'areaStrokeInner', (e: any) => {
      if ( e.features && e.features[0] && e.features[0].properties.catSlug !== 'rock-area') {
        this.map.getCanvas().style.cursor = '';
      }
    });
    this.map.on('click', 'areaStrokeInner', (e: any) => {
      const geom = e.features[0].geometry.coordinates;
      // var coordinates = this.getCentroidBase( geom ).slice();
      const coordinates = geom[0][0].slice();
      const props = e.features[0].properties;
      if ( props.catSlug === 'rock-area' ) {
        return;
      }

      while ( Math.abs( e.lngLat.lng - coordinates[0] ) > 180 ) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }

      let html = '';
      html += '<div class="popup-inner">';
        html += '<h1>' + props.name + '</h1>';
        html += '<h3 style="color: ' + props.color + '">' + props.catName + '</h3>';
        if ( props.summary ) {
          html += '<p class="popup-summary">' + props.summary + '</p>';
        }
        if ( props.description ) {
          html += '<p class="popup-description">' + props.description + '</p>';
        }
      html += '</div>';

      this.currentAreaPopup = new mapboxgl.Popup()
        .setLngLat( coordinates )
        .setHTML( html )
        .addTo( this.map );
    });

    this.map.on('load', () => { this.getData(); });

    this.map.on('zoomend', () => {
      if ( this.map.getZoom() >= this.cityMarkersVisibilityZoomThreshold ) {
        if ( this.rockMode == true ) {
          this.disableRockMode();
        }
      } else {
        if ( this.rockMode == false ) {
          this.enableRockMode();
        }
      }

      this.contextMarkers.forEach(ctx => {
        const el = ctx.getElement();
        const inner = el.childNodes[0];
        const icon = inner.childNodes[0];

        const zoom = Math.round( this.map.getZoom() );

        if ( zoom < this.itemLevelZoom ) {
          el.style.width = el.style.height = zoom + 'px';
          inner.style.width = inner.style.height = zoom / 2 + 'px';
          icon.style.width = icon.style.height = (zoom / 2) - 4 + 'px';
          icon.style.opacity = 0;
        } else {
          el.style.width = el.style.height = zoom * 3 + 'px';
          inner.style.width = inner.style.height = zoom * 1.5 + 'px';
          icon.style.width = icon.style.height = zoom * 1.5 - 10 + 'px';
          icon.style.opacity = 1;
        }
      });
    });

    this.map.on('dragend', () => {
      if (!this.rockMode) {
        this.findCityByDistance();
      }
    });
  }

  ngOnDestroy() {
    this.citySub.unsubscribe();
    this.modelPracticesSub.unsubscribe();
    this.categoriesSub.unsubscribe();
    this.typesSub.unsubscribe();
    this.actionsSub.unsubscribe();
    this.contextsSub.unsubscribe();
  }

  // DATA //
  getData() {
    this.citySub = this.dataSvc.getList('cities').subscribe(
      (cities) => {
        this.subCounter++;
        this.data.cities = cities;
        this.addCityMarkers( cities );
        this.checkData();
      },
      (err) => console.log( err ),
      () => console.log('cities sub complete')
    );

    this.modelPracticesSub = this.dataSvc.getList('best-practices').subscribe(
      (practices) => {
        this.subCounter++;
        this.data.practices = practices;
        this.addPracticeMarkers( practices );
        this.checkData();
      },
      (err) => console.log( err ),
      () => console.log('practices sub complete')
    );

    this.categoriesSub = this.dataSvc.getList('rock-categories').subscribe(
      (categories) => {
        this.subCounter++;
        this.data.categories = categories;
        this.checkData();
      },
      (err) => console.log( err ),
      () => console.log('cities sub complete')
    );

    this.typesSub = this.dataSvc.getList('rock-category-types').subscribe(
      (types) => {
        this.subCounter++;
        this.data.types = types;
        this.checkData();
      },
      (err) => console.log( err ),
      () => console.log('cities sub complete')
    );

    this.actionsSub = this.dataSvc.getList('actions').subscribe(
      (actions) => {
        this.subCounter++;
        this.data.actions = actions;
        this.addActionMarkers( actions );
        this.checkData();
      },
      (err) => console.log( err ),
      () => console.log('actions sub complete')
    );

    this.contextsSub = this.dataSvc.getList('cultural-contexts').subscribe(
      (contexts) => {
        this.subCounter++;
        this.data.contexts = contexts;
        this.addContexts( contexts );
        this.checkData();
      },
      (err) => console.log( err ),
      () => console.log('contexts sub complete')
    );
  }

  checkData() {
    if (this.subCounter === this.numSubs) {
      this.loading = false;
    }
  }

  getCategoryByKey(key: string) {
    return this.data.categories.filter((cat: any) => cat.key === key);
  }

  getTypeByKey(key: string) {
    return this.data.types.filter((type: any) => type.key === key);
  }

  getActionByKey(key: string) {
    return this.data.actions.filter((action: any) => action.key === key);
  }

  // ROCK MODE //
  enableRockMode() {
    console.log('enable rock mode');
    this.rockMode = true;

    this.cityMarkers.forEach(marker => {
      marker.getElement().classList.remove('hidden');
    });

    this.actionMarkers.forEach(marker => {
      marker.getElement().classList.add('hidden');
      marker.getElement().classList.add('switched-off');
    });

    this.contextMarkers.forEach(marker => {
      marker.getElement().classList.add('hidden');
      marker.getElement().classList.add('switched-off');
    });

    this.practiceMarkers.forEach(marker => {
      marker.getElement().classList.add('hidden');
      marker.getElement().classList.add('switched-off');
    });

    this.map.setLayoutProperty('areaFill', 'visibility', 'none');
    this.map.setLayoutProperty('areaStroke', 'visibility', 'none');
    this.map.setLayoutProperty('areaStrokeInner', 'visibility', 'none');

    this.currentCity = undefined;
  }

  disableRockMode() {
    console.log('disable rock mode');
    this.rockMode = false;

    this.cityMarkers.forEach((marker: any) => {
      if (!marker.getElement().classList.contains('model-city-marker')) {
        marker.getElement().classList.add('hidden');
      }
    });

    this.actionMarkers.forEach(marker => {
      marker.getElement().classList.remove('hidden');
      marker.getElement().classList.remove('switched-off');
    });

    this.contextMarkers.forEach(marker => {
      marker.getElement().classList.remove('hidden');
      marker.getElement().classList.remove('switched-off');
    });

    this.practiceMarkers.forEach(marker => {
      marker.getElement().classList.remove('hidden');
      marker.getElement().classList.remove('switched-off');
    });

    this.map.setLayoutProperty('areaFill', 'visibility', 'visible');
    this.map.setLayoutProperty('areaStroke', 'visibility', 'visible');
    this.map.setLayoutProperty('areaStrokeInner', 'visibility', 'visible');
    this.map.setFilter('areaFill');
    this.map.setFilter('areaStroke');
    this.map.setFilter('areaStrokeInner');

    if ( !this.currentCity ) {
      this.findCityByDistance();
    }

    if (!this.showingItem) {
      this.buildSidebar();
    }
  }

  // MARKERS //
  addDefaultLocationMarker(size: number = 30, color: string = 'red', radius: number = 50) {
    const el = document.createElement('div');
    el.style.width = size + 'px';
    el.style.height = size + 'px';
    el.style.backgroundColor = color;
    el.style.borderRadius = radius + '%';

    const marker = new mapboxgl.Marker(el);
    marker.setLngLat( this.defaultLocation )
    .addTo( this.map );
  }

  addCityMarkers( cities: any ) {
    cities.forEach((city: any) => {
      const el = document.createElement('div');
      el.id = 'marker-' + city.key;
      el.classList.add('city-marker');
      el.classList.add( city.type + '-city-marker' );

      let offset = [ 0, -20 ];
      let popupOffset = 25;
      let mainColor = '#9EA1A1';
      if ( city.type == 'replicator' ) {
        offset = [ 0, -30 ];
        popupOffset = 40;
        mainColor = '#E31A2D';
      }

      let html = '';
      html += '<div class="popup-inner">';
        if ( city.images && city.images.length > 0 ) {
          html += '<img width="100" height="100" src="' + city.images[0].secure_url + '">';
        }
        html += '<h1 style="color: ' + mainColor + '">' + city.name + '</h1>';
        html += '<p class="city-population">Population: <strong>' + city.population + '</strong></p>';
        if ( city.description ) {
          html += '<div class="city-description">' + city.description + '</div>';
        }
      html += '</div>';

      const popup = new mapboxgl.Popup({
        offset: popupOffset,
        className: 'popup city-popup'
      })
      .setHTML( html );

      const marker = new mapboxgl.Marker(el, { offset: offset })
      .setLngLat([ city.geolocation.lng, city.geolocation.lat ])
      .setPopup(popup)
      .addTo(this.map);

      this.cityMarkers.push( marker );
    });
  }

  addActionMarkers( actions: any ) {
    actions.forEach((action: any) => {
      if ( action.type === 'point' ) {
        const el = document.createElement('div');
        el.id = 'marker-' + action.key;
        el.classList.add('action-marker');
        el.classList.add('action-cat' + action.ROCKcategory);
        el.classList.add('action-cat-type' + action.ROCKtype);
        el.classList.add('action-item' + action.key);
        el.classList.add('hidden');

        const elInner = document.createElement('div');
        elInner.classList.add('marker-inner');

        let color = 'white';

        this.data.categories.forEach(cat => {
          if ( cat.key === action.ROCKcategory ) {
            color = cat.color;
          }
        });

        this.data.types.forEach((type: any) => {
          // console.log(type.key === action.ROCKtype);
          if ( type.key === action.ROCKtype ) {
            if (action.status === 'full') {
              elInner.innerHTML = this.symbolsSvc.getSymbol( type.symbol, color )
            }
            if (action.status === 'empty') {
              elInner.innerHTML = this.symbolsSvc.getSymbol( type.symbol, color, '0', color, '1', '5' )
            }
          }
        });

        el.appendChild( elInner );

        el.addEventListener('click', (e: Event) => {
          e.preventDefault();
          e.stopPropagation();

          const actionID = (<HTMLDivElement>e.currentTarget).id.replace('marker-', '');
          this.sidebarOpen = true;
          this.showItemCard( e, this.getActionByKey( actionID )[0] );
        });

        const marker = new mapboxgl.Marker(el)
        .setLngLat([ action.geolocation.lng, action.geolocation.lat ])
        .addTo(this.map);

        this.actionMarkers.push( marker );
      }

      if ( action.type === 'multiPoints' && action.points && action.points.length > 0 ) {
        action.points.forEach(point => {
          const el = document.createElement('div');
          el.id = 'marker-' + action.key;
          el.classList.add('action-marker');
          el.classList.add('action-cat' + action.ROCKcategory);
          el.classList.add('action-cat-type' + action.ROCKtype);
          el.classList.add('action-item' + action.key);
          el.classList.add('hidden');

          const elInner = document.createElement('div');
          elInner.classList.add('marker-inner');

          let color = 'white';

          this.data.categories.forEach(cat => {
            if ( cat.key === action.ROCKcategory ) {
              color = cat.color;
            }
          });

          this.data.types.forEach(type => {
            if ( type.key === action.ROCKtype ) {
              if (action.status === 'full') {
                elInner.innerHTML = this.symbolsSvc.getSymbol( type.symbol, color )
              }
              if (action.status === 'empty') {
                elInner.innerHTML = this.symbolsSvc.getSymbol( type.symbol, color, '0', color, '1', '5' )
              }
            }
          });

          el.appendChild( elInner );

          el.addEventListener('click', (e: Event) => {
            e.preventDefault();
            e.stopPropagation();

            const actionID = (<HTMLDivElement>e.currentTarget).id.replace('marker-', '');
            this.sidebarOpen = true;
            this.showItemCard( e, this.getActionByKey( actionID )[0] );
          });

          let pointPosition;
          if ( point.location ) {
            pointPosition = [ point.location.longitude, point.location.latitude ];
          } else {
            pointPosition = [ point[0], point[1] ];
          }

          const marker = new mapboxgl.Marker(el)
          .setLngLat( pointPosition )
          .addTo(this.map);

          this.actionMarkers.push( marker );
        });
      }

      // if ( action.type === 'areaStroke' || action.type === 'areaFill' ) {
      //   console.log(action);
      // }
    });
  }

  addContexts( contexts: any ) {
    const strokeAreaFeaturesCollection: any = [];
    const fillAreaFeaturesCollection: any = [];
    let contextCat: any;
    let feature: any;

    contexts.forEach((context: any) => {
      contextCat = this.getCategoryByKey( context.ROCKcategory )[0];

      switch ( context.type ) {
        case 'point':
          feature = {
            'type': 'Feature',
            'properties': {
              color: contextCat.color,
              catName: contextCat.name,
              catSlug: contextCat.slug
            },
            'geometry': {
              'type': 'Point',
              'coordinates': [context.geolocation.lng, context.geolocation.lat]
            }
          };
          this.addContextPoint( context );
          break;
        case 'areaStroke':
          feature = {
            'type': 'Feature',
            'properties': {
              color: contextCat.color,
              catName: contextCat.name,
              catSlug: contextCat.slug,
              name: context.name,
              summary: context.summary,
              description: context.description
            },
            'geometry': {
              'type': 'Polygon',
              'coordinates': [context.area]
            }
          };
          if (contextCat.slug.indexOf('rock-area') > -1) {
            feature.properties.lineW = 10;
          } else {
            feature.properties.lineW = 2;
          }

          strokeAreaFeaturesCollection.push( feature );
          break;
        case 'areaFill':
          feature = {
            'type': 'Feature',
            'properties': {
              color: contextCat.color,
              catName: contextCat.name,
              catSlug: contextCat.slug,
              name: context.name,
              summary: context.summary,
              description: context.description
            },
            'geometry': {
              'type': 'Polygon',
              'coordinates': [context.area]
            }
          };
          fillAreaFeaturesCollection.push( feature );
          break;
      }
    });

    if ( this.map.getLayer('areaStrokeInner') ) {
      this.map.removeLayer('areaStrokeInner');
    }
    this.map.addLayer({
      'id': 'areaStrokeInner',
      'type': 'fill',
      'source': {
        type: 'geojson',
        data: {
          'type': 'FeatureCollection',
          'features': strokeAreaFeaturesCollection
        }
      },
      'paint': {
        'fill-color': ['get', 'color'],
        'fill-opacity': 0
      }
    }, 'road-rail');

    if ( this.map.getLayer('areaFill') ) {
      this.map.removeLayer('areaFill');
    }
    this.map.addLayer({
      'id': 'areaFill',
      'type': 'fill',
      'source': {
        type: 'geojson',
        data: {
          'type': 'FeatureCollection',
          'features': fillAreaFeaturesCollection
        }
      },
      'paint': {
        'fill-color': ['get', 'color'],
        'fill-opacity': 0.75
      }
    }, 'road-rail');

    if ( this.map.getLayer('areaStroke') ) {
      this.map.removeLayer('areaStroke');
    }
    this.map.addLayer({
      'id': 'areaStroke',
      'type': 'line',
      'source': {
        type: 'geojson',
        data: {
          'type': 'FeatureCollection',
          'features': strokeAreaFeaturesCollection
        }
      },
      'paint': {
        'line-color': ['get', 'color'],
        'line-width': ['get', 'lineW']
      }
    }, 'road-rail');

    this.map.setLayoutProperty('areaFill', 'visibility', 'none');
    this.map.setLayoutProperty('areaStroke', 'visibility', 'none');
    this.map.setLayoutProperty('areaStrokeInner', 'visibility', 'none');
  }

  addContextPoint( context: any ) {
    const el = document.createElement('div');
    el.id = 'marker-' + context.key;
    el.classList.add('context-marker');
    el.classList.add('hidden');
    el.classList.add('context-cat' + context.ROCKcategory);

    const elInner = document.createElement('div');
    elInner.classList.add('marker-inner');
    this.data.categories.forEach(cat => {
      if ( cat.key === context.ROCKcategory ) {
        elInner.style.backgroundColor = cat.color;

        const elIcon = document.createElement('img');
        elIcon.src = cat.icon.secure_url;
        elIcon.width = elIcon.height = 10;
        elInner.appendChild( elIcon );
      }
    });

    el.appendChild( elInner );

    const cat = this.getCategoryByKey( context.ROCKcategory );

    let html = '';
    html += '<div class="popup-inner">';
      html += '<h1>' + context.name + '</h1>';
      html += '<h3 style="color: ' + cat[0].color + '">' + cat[0].name + '</h3>';
      if ( context.summary ) {
        html += '<p class="popup-summary">' + context.summary + '</p>';
      }
      if ( context.description ) {
        html += '<p class="popup-description">' + context.description + '</p>';
      }
    html += '</div>';

    const popup = new mapboxgl.Popup({
      offset: 5,
      className: 'popup context-popup'
    })
    .setHTML( html );

    const marker = new mapboxgl.Marker(el)
    .setLngLat([ context.geolocation.lng, context.geolocation.lat ])
    .addTo( this.map )
    .setPopup( popup );

    this.contextMarkers.push( marker );
  }

  addPracticeMarkers( practices: any ) {
    practices.forEach((practice: any) => {
      const el = document.createElement('div');
      el.id = 'marker-' + practice.key;
      el.classList.add('practice-marker');
      el.classList.add('practice-item' + practice.key);
      el.classList.add('hidden');

      const elInner = document.createElement('div');
      elInner.classList.add('marker-inner');

      el.appendChild( elInner );

      let html = '';
      html += '<div class="popup-inner">';
        if ( practice.images && practice.images.length > 0 ) {
          html += '<img width="100" height="100" src="' + practice.images[0].secure_url + '">';
        }
        html += '<h1 style="color: #9EA1A1">' + practice.name + '</h1>';
        if ( practice.summary ) {
          html += '<div class="practice-summary">' + practice.summary + '</div>';
        }
        if (practice.links && practice.links.length > 0) {
          html += '<ul class="practice-links">';
          practice.links.forEach((link) => {
            let destUrl = link.url;
            if (destUrl.indexOf('http') === -1) {
              destUrl = 'https://' + destUrl;
            }

            html += '<li class="practice-link-item">';
              html += '<a class="practice-link" href="' + destUrl + '" title="' + link.label + '" target="blank">' + link.label + '</a>';
            html += '</li>';
          });
          html += '</ul>';
        }
      html += '</div>';

      const popup = new mapboxgl.Popup({
        offset: 10,
        className: 'popup practice-popup'
      })
      .setHTML( html );

      const marker = new mapboxgl.Marker(el)
        .setLngLat([ practice.geolocation.lng, practice.geolocation.lat ])
        .setPopup(popup)
        .addTo(this.map);

      this.practiceMarkers.push( marker );
    });
  }

  toggleFeature(args) {
    switch ( args.length ) {
      case 1:
        this.switches[ args[0] ].children.forEach((catSwitch, i) => {
          const cat = this.dataTree[ args[0] ].categories[i];
          if ( catSwitch.off ) {
            this.hideFeaturesByCategory(cat.key, cat.slug)
          } else {
            this.showFeaturesByCategory(cat.key, cat.slug)
          }
        });
        break;
      case 2:
        const cat = this.dataTree[ args[0] ].categories[ args[1] ];
        if ( this.switches[ args[0] ].children[ args[1] ].off ) {
          this.hideFeaturesByCategory(cat.key, cat.slug);
        } else {
          this.showFeaturesByCategory(cat.key, cat.slug);
        }
        break;
      case 3:
        const type = this.dataTree[ args[0] ].categories[ args[1] ].types[ args[2] ];
        if ( this.switches[ args[0] ].children[ args[1] ].children[ args[2] ].off ) {
            this.hideFeaturesByType(type.key);
        } else {
            this.showFeaturesByType(type.key);
        }
        break;
    }
  }

  hideFeaturesByCategory(catId: string, catSlug: string) {
    this.contextMarkers.forEach(ctxmrk => {
      if ( ctxmrk.getElement().classList.value.indexOf( catId ) > -1 ) {
          ctxmrk.getElement().classList.add( 'switched-off' );
      }
    });

    this.actionMarkers.forEach(actmrk => {
      if ( actmrk.getElement().classList.value.indexOf( catId ) > -1 ) {
          actmrk.getElement().classList.add( 'switched-off' );
      }
    });

    if ( this.hiddenSlugs.indexOf( catSlug ) == -1 ) {
      this.hiddenSlugs.push( catSlug );
    }

    this.map.setFilter('areaFill', [ 'match', ['get', 'catSlug'], this.hiddenSlugs, false, true ]);
    this.map.setFilter('areaStroke', [ 'match', ['get', 'catSlug'], this.hiddenSlugs, false, true ]);
    this.map.setFilter('areaStrokeInner', [ 'match', ['get', 'catSlug'], this.hiddenSlugs, false, true ]);
  }

  showFeaturesByCategory(catId: string, catSlug: string) {
    this.contextMarkers.forEach(ctxmrk => {
      if ( ctxmrk.getElement().classList.value.indexOf( catId ) > -1 ) {
          ctxmrk.getElement().classList.remove( 'switched-off' );
      }
    });

    this.actionMarkers.forEach(actmrk => {
      if ( actmrk.getElement().classList.value.indexOf( catId ) > -1 ) {
          actmrk.getElement().classList.remove( 'switched-off' );
      }
    });

    this.hiddenSlugs.splice( this.hiddenSlugs.indexOf( catSlug ), 1 );
    if (this.hiddenSlugs.length == 0) {
      this.map.setFilter('areaFill');
      this.map.setFilter('areaStroke');
      this.map.setFilter('areaStrokeInner');
    } else {
      this.map.setFilter('areaFill', [ 'match', ['get', 'catSlug'], this.hiddenSlugs, false, true ]);
      this.map.setFilter('areaStroke', [ 'match', ['get', 'catSlug'], this.hiddenSlugs, false, true ]);
      this.map.setFilter('areaStrokeInner', [ 'match', ['get', 'catSlug'], this.hiddenSlugs, false, true ]);
    }
  }

  showAllFeatures() {
    this.contextMarkers.forEach(ctxmrk => {
      ctxmrk.getElement().classList.remove( 'switched-off' );
    });

    this.actionMarkers.forEach(actmrk => {
      actmrk.getElement().classList.remove( 'switched-off' );
    });

    this.hiddenSlugs = [];
    this.map.setFilter('areaFill');
    this.map.setFilter('areaStroke');
    this.map.setFilter('areaStrokeInner');
  }

  hideFeaturesByType(typeId: string) {
    this.actionMarkers.forEach(actmrk => {
      if ( actmrk.getElement().classList.value.indexOf( typeId ) > -1 ) {
          actmrk.getElement().classList.add( 'switched-off' );
      }
    });
  }

  showFeaturesByType(typeId: string) {
    this.actionMarkers.forEach(actmrk => {
      if ( actmrk.getElement().classList.value.indexOf( typeId ) > -1 ) {
          actmrk.getElement().classList.remove( 'switched-off' );
      }
    });
  }

  highlightMarker(key: string, color: string) {
    this.actionMarkers.forEach(actmrk => {
      const el = actmrk.getElement();
      el.style.borderColor = 'none';
      el.classList.remove( 'highlighted' );

      if ( el.classList.value.indexOf( key ) > -1 ) {
          el.classList.add( 'highlighted' );
          el.style.borderColor = color;
      }
    });

    this.practiceMarkers.forEach(prctmrk => {
      const el = prctmrk.getElement();
      el.style.borderColor = 'none';
      el.classList.remove( 'highlighted' );

      if ( el.classList.value.indexOf( key ) > -1 ) {
          el.classList.add( 'highlighted' );
          el.style.borderColor = color;
      }
    });
  }

  unHighlightMarkers() {
    this.actionMarkers.forEach(actmrk => {
      const el = actmrk.getElement();
      el.style.borderColor = 'none';
      el.classList.remove( 'highlighted' );
    });
  }

  // SIDEBAR //
  toggleSidebar() {
    this.sidebarOpen = !this.sidebarOpen;
    this.mapResizing = true;

    if (!this.sidebarOpen) {
      this.sidebar2Open = false;
      this.unHighlightMarkers();
    }
  }

  showSidebar2() {
    if (!this.sidebar2Open) {
      this.mapResizing = true;
    }

    this.sidebar2Open = true;
  }

  hideSidebar2() {
    this.sidebar2Open = false;
    this.unHighlightMarkers();
  }

  buildSidebar() {
    console.log( 'build sidebar' );
    this.switches = [];
    this.collapses = [];
    this.selects = [false, false, false];
    this.hiddenSlugs = [];

    this.dataTree.forEach((element, i) => {
      let prevCategory, prevType;
      const tempData: any = {};

      this.switches.push({
        off: false,
        children: []
      });

      this.collapses.push({
        collapsed: true,
        children: []
      });

      const sorted = this.data[element.name]
      .filter( (item: any) => item.city === this.currentCity.key )
      .sort( this.fieldSorter( [ 'ROCKcategory', 'ROCKtype' ] ) )
      .map(
        (item) => {
          if ( prevCategory !== item.ROCKcategory ) {
            tempData[ item.ROCKcategory ] = [];
          }

          if ( item.ROCKtype && item.ROCKtype !== 'default' ) {
            if ( prevType !== item.ROCKtype ) {
              tempData[ item.ROCKcategory ][ item.ROCKtype ] = [];
            }

            tempData[ item.ROCKcategory ][ item.ROCKtype ].push( item );
            prevType = item.ROCKtype;
          } else {
            tempData[ item.ROCKcategory ].push( item );
          }

          prevCategory = item.ROCKcategory;
        }
      );

      const tempData2: any = [];
      const elementIndex = i;
      let catIndex = 0;

      for (const i in tempData) {
        this.switches[elementIndex].children.push({
          off: false,
          children: []
        });

        this.collapses[elementIndex].children.push({
          collapsed: true,
          children: []
        });

        let typeIndex: number = 0;
        const cat = this.getCategoryByKey( i );
        cat[0].types = [];

        for (const j in tempData[i]) {
          this.switches[elementIndex].children[catIndex].children.push({
            off: false
          });

          this.collapses[elementIndex].children[catIndex].children.push({
            collapsed: true
          });

          const type = this.getTypeByKey( j );
          if ( type[0] ) {
            type[0].items = tempData[ i ][ type[ 0 ].key ];
            cat[0].types[ typeIndex ] = type[0];
            typeIndex++;
          } else {
            cat[0].items = tempData[i];
          }
        }

        tempData2[ catIndex ] = cat[0];
        catIndex++;
      }

      this.dataTree[i].categories = tempData2;
    });

    this.hideSidebar2();
  }

  toggleLevel(evt: MouseEvent, ...args: number[]) {
    evt.stopPropagation();

    switch ( args.length ) {
      case 1:
        this.switches[ args[0] ].off = !this.switches[ args[0] ].off;

        this.switches.forEach((parent, i) => {
          if ( parent.children.length > 0 ) {
            parent.children.forEach((firstLevelChild, j) => {
              this.setLevel(evt, parent.off, i, j);

              if (firstLevelChild.children.length > 0) {
                firstLevelChild.children.forEach((secondLevelChild, z) => {
                  this.setLevel(evt, firstLevelChild.off, i, j, z);
                });
              }
            });
          }
        });

        break;
      case 2:
        this.switches[ args[0] ].children[ args[1] ].off = !this.switches[ args[0] ].children[ args[1] ].off;

        this.switches.forEach((parent, i) => {
          if ( parent.children.length > 0 ) {
            parent.children.forEach((firstLevelChild, j) => {
              if (firstLevelChild.children.length > 0) {
                firstLevelChild.children.forEach((secondLevelChild, z) => {
                  this.setLevel(evt, firstLevelChild.off, i, j, z);
                });
              }
            });
          }
        });

        let numOff: number = 0;

        this.switches[ args[0] ].children.forEach(child => {
          if ( child.off ) {
            numOff++;
          }
        });

        if ( this.switches[ args[0] ].children.length == numOff ) {
          this.switches[ args[0] ].off = true;
        } else {
          this.switches[ args[0] ].off = false;
        }
        break;
      case 3:
        this.switches[ args[0] ].children[ args[1] ].children[ args[2] ].off = !this.switches[ args[0] ].children[ args[1] ].children[ args[2] ].off;

        let numOff2: number = 0;

        this.switches[ args[0] ].children[ args[1] ].children.forEach(child => {
          if ( child.off ) {
            numOff2++;
          }
        });

        if ( this.switches[ args[0] ].children[ args[1] ].children.length == numOff2 ) {
          this.switches[ args[0] ].children[ args[1] ].off = true;
        } else {
          this.switches[ args[0] ].children[ args[1] ].off = false;
        }
        break;
    }

    this.toggleFeature(args);
  }

  setLevel(evt: MouseEvent, isOff: boolean, ...args: number[]) {
    evt.stopPropagation();

    switch ( args.length ) {
      case 1:
        this.switches[ args[0] ].off = isOff;
        break;
      case 2:
        this.switches[ args[0] ].children[ args[1] ].off = isOff;
        break;
      case 3:
        this.switches[ args[0] ].children[ args[1] ].children[ args[2] ].off = isOff;
        break;
    }
  }

  toggleCollapse(...args: number[]) {
    switch ( args.length ) {
      case 1:
        for (let i = 0; i < this.collapses.length; i++) {
          if (args[0] == i) {
            this.selects[i] = !this.selects[i];
            this.collapses[i].collapsed = !this.collapses[i].collapsed
          } else {
            this.selects[i] = false;
            this.collapses[i].collapsed = true;
          }
        }
        break;
      case 2:
        this.collapses[ args[0] ].children[ args[1] ].collapsed = !this.collapses[ args[0] ].children[ args[1] ].collapsed;
        break;
      case 3:
        this.collapses[ args[0] ].children[ args[1] ].children[ args[2] ].collapsed = !this.collapses[ args[0] ].children[ args[1] ].children[ args[2] ].collapsed;
        break;
    }
  }

  showItemCard(evt: any, item: any) {
    evt.stopPropagation();

    this.showingItem = true;
    this.currentItem = item;

    this.showSidebar2();
    this.highlightMarker( item.key, this.getCategoryByKey( item.ROCKcategory )[0].color );

    const to = setTimeout(() => {
      let destPos = [this.currentItem.geolocation.lng, this.currentItem.geolocation.lat];
      if ( item.type === 'multiPoints' ) {
        destPos = this.getCentroid( item.points );
      }

      this.map.flyTo({
        center: destPos,
        zoom: this.itemLevelZoom
      });

      clearTimeout(to);
    }, 500);
  }

  showItemCardFromTagSearch(item: any) {
    this.data.cities.forEach((city: any) => {
      if ( city.key === item.city ) {
        this.currentCity = city;
      }
    });
    this.buildSidebar();
    this.showAllFeatures();
    this.sidebarOpen = true;

    const evt: MouseEvent = new MouseEvent('click');
    this.showItemCard( evt, item );
  }

  searchByTag(tagLabel: string) {
    this.header.onTyping(tagLabel);
  }

  showItemCardFromRelatedLink(itemID: string) {
    this.data.actions.forEach((action: any) => {
      if ( action.key === itemID ) {
        const evt: MouseEvent = new MouseEvent('click');
        this.showItemCard( evt, action );
      }
    });
  }

  openStoryModal(item: any) {
    this.storyModal.initStory( item.key );
    this.isModalOpen = true;
  }

  closeStoryModal() {
    this.isModalOpen = false;
  }

  enlargeStoryModal() {
    this.isModalLarge = true;
  }

  shrinkStoryModal() {
    this.isModalLarge = false;
  }

  onTransitionEnd(event: TransitionEvent) {
    if (event.propertyName === 'width') {
      this.storyModal.resizeStoryMap();
    }
  }

  // MAP //
  flyTo(evt: any) {
    this.header.closeMobileMenu();
    this.sidebar2Open = false;
    this.showingItem = false;
    this.enableRockMode();

    this.data.cities.forEach(city => {
      if ( city.key === evt.key ) {
        this.currentCity = city;
      }
    });

    if ( this.currentCity.type === 'model' ) {
      this.sidebarOpen = false;
    } else {
      this.sidebarOpen = true;
    }

    let to = setTimeout(() => {
      this.map.flyTo({
        center: [evt.loc.lng, evt.loc.lat],
        zoom: this.cityMarkersVisibilityZoomThreshold + 3,
        duration: 2000
      });

      to = setTimeout(() => {
        this.disableRockMode();
        clearTimeout( to );
      }, 2000);
    }, 500);
  }

  centerMap() {
    this.sidebar2Open = this.sidebarOpen = false;

    const to = setTimeout(() => {
      this.map.resize();

      this.map.flyTo({
        center: this.defaultLocation,
        zoom: this.zoom
      });
      clearTimeout(to);
    }, 500);
  }

  onMapDivTransitionEnd(evt: TransitionEvent) {
    if ( evt.propertyName === 'width' ) {
      this.map.resize();
      const to = setTimeout(() => {
        this.mapResizing = false;
        clearTimeout(to);
      }, 500);
    }
  }

  // SPATIAL HELPERS //
  findCityByDistance() {
    // console.log( 'find city by distance' );
    const center: any = this.map.getCenter();
    const distances: number[] = [];

    this.data.cities.forEach(city => {
      distances.push( distance( [center.lng, center.lat], [city.geolocation.lng, city.geolocation.lat] ) );
    });

    const nearestIndex = distances.indexOf( Math.min(...distances) );

    this.currentCity = this.data.cities[ nearestIndex ];

    // if(!this.showingItem)
    //   this.buildSidebar()
  }

  getCentroid(points: any) {
    const pointsArray = [];
    points.forEach(point => {
      if ( point.location && point.location.longitude && point.location.latitude ) {
        pointsArray.push( [ point.location.longitude, point.location.latitude ] );
      } else {
        pointsArray.push( point );
      }
    });

    const poly = {
      'type': 'Feature',
      'properties': {},
      'geometry': {
        'type': 'Polygon',
        'coordinates': [ pointsArray ]
      }
    };

    return centroid( poly ).geometry.coordinates;
  }

  getCentroidBase(points: any) {
    const poly = {
      'type': 'Feature',
      'properties': {},
      'geometry': {
        'type': 'Polygon',
        'coordinates': points
      }
    };

    return centroid( poly ).geometry.coordinates;
  }

  // ROCK TYPE SYMBOL HELPER //
  getSymbol(name: string, color: string) {
    return this.sanitizer.bypassSecurityTrustHtml( this.symbolsSvc.getSymbol(name, color) );
  }
}
