import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Observable } from 'rxjs';

// import { HeaderComponent } from "../header/header.component";
import { environment } from '../../../environments/environment';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {
  public item: Observable<any>;
  public category: Observable<any>;
  private type: Observable<any>;
  public coverImg = 'url(/assets/img/cover-placeholder.png)';
  public hasCoverImg = false;
  private mbStaticUrl: string;

  public safeURL: SafeResourceUrl;

  constructor(
    private route: ActivatedRoute,
    private dataSvc: DataService,
    private sanitizer: DomSanitizer
  ) {
  }

  ngOnInit() {
    this.item = this.dataSvc.getObject('actions/' + this.route.snapshot.params.id);
    this.item.subscribe(
      (item: any) => {
        console.log( item );

        // this.mbStaticUrl = `
        //   https://api.mapbox.com/styles/v1/mapbox/light-v9/static/
        //   ${ item.geolocation.lng },${ item.geolocation.lat },14.0,0,0/
        //   300x200@2x?access_token=${ environment.mapbox.accessToken }
        // `;

        this.category = this.dataSvc.getObject('rock-categories/' + item.ROCKcategory);
        this.type = this.dataSvc.getObject('rock-types/' + item.ROCKtype);
        if ( item.images && item.images.length > 0 ) {
          this.coverImg = 'url(' + item.images[0].secure_url + ')';
          this.hasCoverImg = true;
        }
      }
    );
  }

  YTUrl(YTID: string) {
    this.safeURL = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + YTID);
    return this.safeURL;
  }
}
