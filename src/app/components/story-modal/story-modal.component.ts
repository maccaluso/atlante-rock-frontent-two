import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import * as mapboxgl from 'mapbox-gl';

import { DataService } from '../../services/data.service';
import { SymbolsService } from '../../services/symbols.service';

@Component({
  selector: 'app-story-modal',
  templateUrl: './story-modal.component.html',
  styleUrls: ['./story-modal.component.scss']
})
export class StoryModalComponent implements OnInit {
  @Output() closeModal: any = new EventEmitter<any>();
  @Output() enlargeModal: any = new EventEmitter<any>();
  @Output() shrinkModal: any = new EventEmitter<any>();

  public item: Observable<any>;
  public category: Observable<any>;
  private type: Observable<any>;
  public coverImg: string;
  public isModalLarge = false;
  public hasStakeHolders = true;
  public hasLinks = true;

  public map: mapboxgl.Map;
  private mapContainerSelector = 'longFormMap';
  private styleUrl = 'mapbox://styles/lopezmaps/cj9fpbnby0dpa2roeayvnkfc5';
  private zoom = 16;

  public safeURL: SafeResourceUrl;

  constructor(
    private dataSvc: DataService,
    private symbolsSvc: SymbolsService,
    private sanitizer: DomSanitizer
  ) {
  }

  ngOnInit() {
    this.initStory('-LEtVmdo5zCw5ipuQtBK');
    // this.safeURL = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/MhIzJrTYHBc');
  }

  initStory(itemID: any) {
    document.getElementById('storyModalScrollContainer').scrollTop = 0;
    this.coverImg = 'url(/assets/img/cover-placeholder.jpg)';

    this.item = this.dataSvc.getObject('actions/' + itemID);
    this.item.subscribe(
      (item: any) => {
        if (!item.stakeholders || item.stakeholders.length === 0) {
          this.hasStakeHolders = false;
        }
        if (!item.links || item.links.length === 0) {
          this.hasLinks = false;
        }
        this.category = this.dataSvc.getObject('rock-categories/' + item.ROCKcategory);
        this.type = this.dataSvc.getObject('rock-category-types/' + item.ROCKtype)

        combineLatest(
          this.category,
          this.type
        ).pipe(
          map(
            ([cat, type]) => {
              return {
                category: cat,
                categoryType: type
              };
            }
          )
        )
        .subscribe(
          (data: any) => {
            if ( item.images && item.images.length > 0 ) {
              this.coverImg = 'url(' + item.images[0].secure_url + ')';
            }

            this.initStoryMap(item.geolocation, item.status, data);
          },
          (err: any) => console.log(err)
        );
      }
    );
  }

  initStoryMap(geolocation: any, status: string, data: any) {
    this.map = new mapboxgl.Map({
      container: this.mapContainerSelector,
      style: this.styleUrl,
      zoom: this.zoom,
      center: [geolocation.lng, geolocation.lat],
      interactive: false
    });

    const el = document.createElement('div');
    el.style.width = '30px';
    el.style.height = '30px';
    if (status === 'full') {
      el.innerHTML = this.symbolsSvc.getSymbol( data.categoryType.symbol, data.category.color );
    }
    if (status === 'empty') {
      el.innerHTML = this.symbolsSvc.getSymbol( data.categoryType.symbol, data.category.color, '0', data.category.color, '1', '5' );
    }

    const marker = new mapboxgl.Marker(el);
    marker
      .setLngLat( [geolocation.lng, geolocation.lat] )
      .addTo( this.map );

    // this.map.on('resize', (e: any) => {
    //   console.log(e);
    // });
  }

  resizeStoryMap() {
    this.map.resize();
  }

  closeStoryModal() {
    this.closeModal.emit();
  }

  enlargeStoryModal() {
    this.isModalLarge = true;
    this.enlargeModal.emit();
  }

  shrinkStoryModal() {
    this.isModalLarge = false;
    this.shrinkModal.emit();
  }

  YTUrl(YTID: string) {
    this.safeURL = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + YTID);
    return this.safeURL;
  }
}
