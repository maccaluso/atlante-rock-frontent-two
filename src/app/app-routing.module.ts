import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapComponent } from "./components/map/map.component";
import { StoryComponent } from "./components/story/story.component";

const routes: Routes = [
  { path: '', component: MapComponent },
  { path: 'story/:id', component: StoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
