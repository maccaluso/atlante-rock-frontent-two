export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyDq112LMAgKXcqU1xqz2eQkPH4x0aQ9Bwg",
    authDomain: "atlante-rock.firebaseapp.com",
    databaseURL: "https://atlante-rock.firebaseio.com",
    projectId: "atlante-rock",
    storageBucket: "atlante-rock.appspot.com",
    messagingSenderId: "128304408510"
  },
  mapbox: {
    accessToken: 'pk.eyJ1IjoibG9wZXptYXBzIiwiYSI6ImNqNDB3YnJtcDAxNDMzMnMyNWZmMGZ3a3IifQ.2VbIS4DW-DHklfXmg1vlKw'
  },
  cloudinary: {
    cloud_name: 'gianluca-macaluso', 
    upload_preset: 'ROCK-Atlas'
  }
};
