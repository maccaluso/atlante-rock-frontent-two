import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() cities: any;
  @Input() actions: any;
  @Input() isBack = false;
  @Input() isModal = false;
  @Output() jump: any = new EventEmitter<any>();
  @Output() highlight: any = new EventEmitter<any>();

  private search: string;
  private showCityList: boolean ;
  private filteredActions: any;
  public mobileMenuOpen = false;

  constructor() {
    this.search = '';
  }

  ngOnInit() {
    this.showCityList = false;
  }

  toggleCityList() {
    this.showCityList = !this.showCityList;
  }

  jumpTo(e: MouseEvent, city: any) {
    e.stopPropagation();
    this.showCityList = false;
    this.jump.emit( {loc: city.geolocation, key: city.key} );
  }

  onTyping(evt: any) {
    this.search = evt;
    this.showCityList = false;

    if ( evt.length === 0 ) {
      this.filteredActions = [];
      return;
    }

    this.filteredActions = this.actions.filter(
      (action: any) => {
        let hasTag = false;
        if ( action.tags ) {
          action.tags.forEach(tag => {
            tag.hasMatch = false;

            if ( tag.label.toLowerCase().indexOf( evt.toLowerCase() ) > -1 ) {
              hasTag = true;
              tag.hasMatch = true;
            }
          });
        }

        return hasTag;
      }
    );
  }

  clearSearch() {
    this.search = '';
  }

  onSearchFieldBlur() {
    this.search = '';
    this.filteredActions = [];
  }

  highlightAction(action: any) {
    this.highlight.emit( action );
    this.search = '';
    this.filteredActions = [];
  }

  toggleMobileMenu(e: MouseEvent) {
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
    this.mobileMenuOpen = !this.mobileMenuOpen;
    this.showCityList = false;
  }

  closeMobileMenu() {
    this.mobileMenuOpen = false;
    this.showCityList = false;
  }

}
